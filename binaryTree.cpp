//обращаем внимание на отсутствие .h
#include <iostream>

//не используем заголовочные файлы для C
//#include <stdio.h>
//#include <stdlib.h>

//"ленивый стиль" - подключаем пространство имён целиком
using namespace std;

//класс узла бинарного дерева
//содержит указатели на поддеревья,
//что обеспечивает связность дерева
class BinaryTreeNode
{
private:
	int data;					//информационное поле, играющее роль ключа
	BinaryTreeNode* left;		//указатель на узел, являющийся корнем левого поддерева
	BinaryTreeNode* right;		//указатель на узел, являющийся корнем правого поддерева
public:
	BinaryTreeNode(int value);	//конструктор
	int getData() const;		//публичный метод для чтения приватного поля data
	void addNode(int value);	//добавление узла
	BinaryTreeNode* searchNode(int value);	//поиск узла по ключу
	BinaryTreeNode* removeNode(int value);		//удаление узла по ключу
	BinaryTreeNode* getMinNode();	//поиск узла с минимальным значением поля data
	BinaryTreeNode* getMaxNode();	//поиск узла с максимальным значением поля data
	void printSortAscending() const;			//вывод содержимого дерева по возрастанию
	void printSortDescending() const;			//вывод содержимого дерева по убыванию
	~BinaryTreeNode();			//деструктор
};

//класс бинарного дерева
//содержит только указатель
//узел - корень дерева
class BinaryTree
{
private:
	BinaryTreeNode* root;			//указатель на узел, являющийся корнем дерева
public:
	BinaryTree();					//конструктор
	bool isEmpty() const;			//проверка - дерево пусто?
	void add(int value);			//добавление нового элемента
	bool search(int value) const;	//проверка - есть ли элемент с заданным ключом?
	bool remove(int value);			//удаление элемента с заданным ключом (если он есть)
	int getMin() const;				//получение минимального ключа
	int getMax() const;				//получение максимального ключа
	void print(bool desc = false) const;	//вывод содержимого дерева на печать (по умолчанию параметр "по убыванию" выставлен в false
	~BinaryTree();					//деструктор

	//для демонстрационных целей!
	//обозначним перегрузку оператора <<
	//как дружественную функцию
	friend ostream& operator<< (ostream& os, const BinaryTree& tree);
};

void work_with_tree();		//бинарное дерево будет локальным объектом в рамках этой функции

int main()
{
	work_with_tree();
	getchar();
	return 0;
}

void work_with_tree()
{
	BinaryTree tree;		//объект дерева - локальный в рамках этой функции
	//помните - неявно вызывается конструктор!
	//начинается жизненный цикл объекта

	//формируем дерево
	tree.add(5);
	tree.add(-3);
	tree.add(10);
	tree.add(8);
	tree.add(6);
	tree.add(3);
	tree.add(-7);
	tree.add(-5);
	tree.add(0);
	tree.add(20);

	//проверяем работу функций поиска минимума и максимума
	cout << "min = " << tree.getMin() << endl;
	cout << "max = " << tree.getMax() << endl;

	//проверяем работу функций обхода по дереву
	tree.print();
	tree.print(true);
	//проверяем работу перегруженного вывода
	cout << tree;

	//проверяем работу перегруженного вывода при выводе нескольких значений
	cout << "Test cout <<" << endl << tree << "Operator<< overload works!" << endl;
		
	//проверяем корректность удаления
	tree.remove(10);
	cout << tree;

	getchar();
	return;		//неявно вызывается деструктор для объекта бинарного дерева
	//с завершением функции завершается и жизненный цикл объекта
}

/*методы класса BinaryTreeNode*/
BinaryTreeNode::BinaryTreeNode(int value)
{
	//инициализируем поля класса
	data = value;
	left = nullptr;
	right = nullptr;
	//в стандарте "С++ 11" введено ключевое слово nullptr,
	//которое предпочтительнее использовать, чем NULL
	//(который введён с использованием #define)

	//диагностический вывод
	//cout << "constructor works! " << value << endl;
}

int BinaryTreeNode::getData() const
{
	//распространённый приём:
	//публичный метод возвращает приватное поле
	return data;
}

void BinaryTreeNode::addNode(int value)
{
	//добавляемый элемент должен попасть в правое поддерево
	if (value > data)
		//если правое поддерево уже существует
		if (right)
			//организуем рекурсивный вызов в контексте правого потомка
			right->addNode(value);
		else
			//иначе создаём новый узел и делаем его правым потомком
			right = new BinaryTreeNode(value);
	//ВАЖНО!!!
	//оператор new распределяет динамическую память,
	//но при этом ещё и вызывается конструктор для создаваемого объекта

	//добавляемый элемент должен попасть в левое поддерево
	if (value < data)
		if (left)
			left->addNode(value);
		else
			left = new BinaryTreeNode(value);
}

BinaryTreeNode* BinaryTreeNode::searchNode(int value)
{
	//если нашли, что искали - вернули адрес узла дерева
	if (data == value)
		return this;

	//иначе продолжаем рекурсинвый поиск
	if (value > data)
		if (right)
			return right->searchNode(value);
		else
			return nullptr;
	else
		if (left)
			return left->searchNode(value);
		else
			return nullptr;
}

BinaryTreeNode* BinaryTreeNode::removeNode(int value)
{
	/*
	метод НЕ удаляет какой-либо объект узла!!!
	Но он меняет структуру связей между узлами,
	изолируя удаляемый узел от всех остальных.
	В результате его работы возвращается указатель на узел,
	который подлежит удалению.
	*/

	BinaryTreeNode* toDelete = nullptr;		//указатель на искомый узел

	//если узел принадлежит правому поддереву и оно существует
	if (value > data && right)
	{
		//организуем рекурсивный поиск в контексте правого поддерева
		toDelete = right->removeNode(value);
		//если понимаем, что удалять надо непостредственно правого потомка
		if (toDelete == right)
			//заменяем указатель на него на пустой
			right = nullptr;
	}

	//аналогично с левым поддеревом (не забываем про проверку: если оно существует)
	if (value < data && left)
	{
		toDelete = left->removeNode(value);
		if (toDelete == left)
			left = nullptr;
	}

	//если текущий узел - тот, который нужно удалить
	if (data == value)
	{
		//если узел не имеет потомков (лист)
		if (!left && !right)
			toDelete = this;

		//если у узла есть только левый потомок
		if (left && !right)
		{
			toDelete = left;
			
			//ВАЖНО!!!
			//в данном случае вызывается
			//перегруженный по умолчанию
			//оператор присваивания (=),
			//который реализует стратегию
			//"поверхностного" копирования,
			//т.е. копирования одноимённых
			//полей класса
			*this = *left;
		}

		//если у узла есть только правый потомок
		if (!left && right)
		{
			toDelete = right;
			*this = *right;
		}
		//если у узла есть оба потомка
		if (left && right)
		{
			//внимательно читаем названия переменных
			int max_value_in_left_subtree = left->getMaxNode()->data;
			int min_value_in_right_subtree = right->getMinNode()->data;
			//находим из них то, которое ближе к значению ключа рассматриваемого узла
			if ((data - max_value_in_left_subtree) < (min_value_in_right_subtree - data))
			{
				//не удаляем рассматриваемый элемент - вместо этого
				//удаляем элемент из поддерева, наиболее близкий к нему
				toDelete = left->removeNode(max_value_in_left_subtree);
				//сохраняем ссылочную структуру дерева,
				//но обновляем содержимое информационных полей
				//(в нашем случае - только data)
				data = max_value_in_left_subtree;
			}
			else
			{
				//аналогично для правого поддерева
				toDelete = right->removeNode(min_value_in_right_subtree);
				data = min_value_in_right_subtree;
			}
		}
	}
	
	return toDelete;
}

BinaryTreeNode* BinaryTreeNode::getMinNode()
{
	//рекурсивно движемся влево по дереву
	if (left)
		return left->getMinNode();
	else
		//пока не дойдём до элемента,
		//не имеющего левого потомка
		return this;
}

BinaryTreeNode* BinaryTreeNode::getMaxNode()
{
	if (right)
		return right->getMaxNode();
	else
		return this;
}

void BinaryTreeNode::printSortAscending() const
{
	if (left)
		left->printSortAscending();		//обхдим левое поддерево
	
	cout << data << "  ";			//печатаем содержимое узла

	if (right)
		right->printSortAscending();	//обхдим правое поддерево
}

void BinaryTreeNode::printSortDescending() const
{
	/*
	аналогично предыдущей функции,
	меняется только порядок обхода поддеревьев
	*/

	if (right)
		right->printSortDescending();	//обхдим правое поддерево

	cout << data << "  ";			//печатаем содержимое узла

	if (left)
		left->printSortDescending();		//обхдим левое поддерево
}

BinaryTreeNode::~BinaryTreeNode()
{
	/*
	ВАЖНО!!!
	Надо понимать, что для каждого узла дерева память выделяется динамически.
	Единственное место, где сохраняются адреса объектов узлов дерева -
	это указатели left или right его предка.
	Поэтому при удалении любого узла мы потеряем адреса, позволяющие отыскать потомков.
	В частности, потеряв эти адреса, мы не сможем освободить
	динамически распределённую для объектов потомков память.
	Поэтому при удалении узла надо прежде
	инициировать удаление потомков (если они есть).
	В случае дерева, это приводит к рекурсивному обходу по нему
	с удалением всех просмотренных узлов.
	*/
	if (left)
		delete left;
	if (right)
		delete right;
	
	//диагностический вывод
	//cout << "destructor works! " << data << endl;
}

/*методы класса BinaryTree*/
BinaryTree::BinaryTree()
{
	//изначально дерево пусто,
	//указатель на корень инициализируем
	//как пустой
	root = nullptr;
	
	//диагностический вывод
	cout << "Tree created!!!" << endl;
}

bool BinaryTree::isEmpty()const
{
	return (!root ? true : false);
}

void BinaryTree::add(int value)
{
	//если корень есть
	if (root)
		//добавление узла реализуем
		//с использованием методов класса
		//BinaryTreeNode, вызывая их
		//в контексте объекта корня
		root->addNode(value);
	else
		//иначе создаём новый объект узла дерева
		//и запоминаем его адрес как адрес корня
		root = new BinaryTreeNode(value);
}

bool BinaryTree::search(int value) const
{
	//если корень есть
	if (root)
		return (root->searchNode(value) ? true : false);
	//а если нет - то и искать нечего
	return false;
}

bool BinaryTree::remove(int value)
{
	/*
	Метод возвращает true,
	если узел был найден по ключу и удалён.
	Во всех остальных случаях возвращается false.
	*/

	if (root)
	{
		//ищем узел, предназначенный для удаления
		BinaryTreeNode* toDelete = root->removeNode(value);
		//если такой нашёлся
		if (toDelete)
		{
			//удаляем его
			delete toDelete;
			//если удалили корень
			if (toDelete == root)
				//считаем, что дерево стало пустым
				root = nullptr;
			
			return true;
		}
	}
	return false;
}

int BinaryTree::getMin() const
{
	if (root)
		//обратите внимание,
		//что обращаемся к публичному методу getData
		//для доступа к приватному полю data
		return (root->getMinNode())->getData();
	else
		return 0;
}

int BinaryTree::getMax() const
{
	if (root)
		return (root->getMaxNode())->getData();
	else
		return 0;
}

void BinaryTree::print(bool desc) const
{
	if (root)
		//учитываем порядок вывода
		if (desc)
			root->printSortDescending();
		else
			root->printSortAscending();
	else
		cout << "The tree is empty";
	
	//перевод на новую строку осуществляется
	//при всех трёх возможных исходах
	cout << endl;
}

BinaryTree::~BinaryTree()
{
	//если есть корень дерева
	if (root)
		//необходимо освободить динамически распределённую для него память
		delete root;

	//ВАЖНО!!! Это приведёт к неявному вызову деструктора для объекта корня

	//диагностический вывод
	cout << "The tree deleted!!!" << endl;
}

/*функция перегрузки для обеспечения работы cout*/
ostream& operator<< (ostream& os, const BinaryTree& tree)
{
	/*
	Для демонстрационных целей!
	В данном случае вызов tree.print() опирается на cout,
	а не получает os как параметр, чтобы не усложнять логику.
	*/
	tree.print();
	return os;
}